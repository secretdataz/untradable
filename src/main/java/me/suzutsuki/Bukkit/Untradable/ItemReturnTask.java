package me.suzutsuki.Bukkit.Untradable;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ItemReturnTask implements Runnable {

    Player p;
    ItemStack[] stacks;
    public ItemReturnTask(Player player,ItemStack[] itemStacks)
    {
        p=player;
        stacks=itemStacks;
    }
    @Override
    public void run() {
        p.getInventory().setContents(stacks);
    }
}

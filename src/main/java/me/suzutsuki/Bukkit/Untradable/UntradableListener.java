package me.suzutsuki.Bukkit.Untradable;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;


/**
 * Created by Mizuki on 3/16/14.
 */
public class UntradableListener implements Listener {

    UntradablePlugin pl;
    public UntradableListener(UntradablePlugin plugin)
    {
        pl=plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDrop(PlayerDropItemEvent event)
    {
        if(event.getPlayer().hasPermission("untradable.drop"))return;
        ItemStack item = event.getItemDrop().getItemStack();
        if(item == null)return;
        if(item.getItemMeta() == null)return;
        List<String> lore = item.getItemMeta().getLore();
        if(lore == null)return;
        if(lore.contains(UntradablePlugin.UntradableLore))
        {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInv(InventoryClickEvent event)
    {
        if(event.getWhoClicked().hasPermission("untradable.invclick") || event.getWhoClicked().getGameMode() == GameMode.CREATIVE) return;
        if (event.getView().getTopInventory().getType() == InventoryType.ENDER_CHEST) return;
        if (event.getView().getTopInventory().getType() == InventoryType.MERCHANT) return;
        if (event.getView().getTopInventory().getType() == InventoryType.WORKBENCH) return;
        if (event.getView().getTopInventory().getType() == InventoryType.CRAFTING) return;
        Player p = (Player) event.getWhoClicked();
        ItemStack clicked = event.getCurrentItem();
        if(clicked == null)return;
        ItemMeta meta = clicked.getItemMeta();
        if(meta == null)return;
        List<String> lore = meta.getLore();
        if(lore == null)return;
        if(lore.contains(UntradablePlugin.UntradableLore))
        {
            event.setCancelled(true);
            final Player player = p;
            Bukkit.getScheduler().scheduleSyncDelayedTask(pl,new Runnable() {
                @Override
                public void run() {
                    player.closeInventory();
                }
            });
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEntityEvent event)
    {
        Entity e = event.getRightClicked();
        if(e instanceof ItemFrame)
        {
            Player p = event.getPlayer();
            if(!p.hasPermission("untradable.itemframe"))
            {
                ItemStack stack = p.getItemInHand();
                if(stack == null)return;
                ItemMeta meta = stack.getItemMeta();
                if(meta == null)return;
                List<String> lore = meta.getLore();
                if(lore == null)return;
                if(lore.contains(UntradablePlugin.UntradableLore))
                {
                    event.setCancelled(true);
                }
            }
        }
    }
}

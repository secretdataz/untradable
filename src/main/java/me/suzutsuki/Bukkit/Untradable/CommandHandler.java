package me.suzutsuki.Bukkit.Untradable;

import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class CommandHandler implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender cs, Command command, String s, String[] strings) {
        if(command.getName().equalsIgnoreCase("adminscissors"))
        {
            if(cs instanceof Player)
            {
                Player p = (Player)cs;
                ItemStack stack = p.getItemInHand();
                Validate.notNull(stack);
                if(stack.getType() != Material.AIR)
                {
                    ItemMeta meta = stack.getItemMeta();
                    List<String> lore = meta.getLore();
                    if(lore == null)
                    {
                        p.sendMessage("This item is already tradable.");
                    }
                    lore.remove("Untradable");
                    meta.setLore(lore);
                    stack.setItemMeta(meta);
                    p.sendMessage("Made item in your hand tradable.");
                }
            }
            return true;
        }
        if(command.getName().equalsIgnoreCase("scissors"))
        {
            cs.sendMessage("Not yet implemented");
            return true;
        }
        if(command.getName().equalsIgnoreCase("makeuntradable"))
        {
            if(cs instanceof Player)
            {
                Player p = (Player)cs;
                ItemStack stack = p.getItemInHand();
                Validate.notNull(stack);
                if(stack.getType() != Material.AIR)
                {
                    ItemMeta meta = stack.getItemMeta();
                    List<String> lore = meta.getLore();
                    if(lore == null)lore = new ArrayList<String>();
                    lore.add("Untradable");
                    meta.setLore(lore);
                    stack.setItemMeta(meta);
                    p.sendMessage("Made item in your hand untradable.");
                }
            }
        }
        return false;
    }
}

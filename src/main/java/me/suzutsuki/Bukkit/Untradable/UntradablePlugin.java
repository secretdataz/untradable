package me.suzutsuki.Bukkit.Untradable;

import org.bukkit.plugin.java.JavaPlugin;

public class UntradablePlugin extends JavaPlugin{

    public static String UntradableLore = "Untradable";

    public void onEnable()
    {
        getServer().getPluginManager().registerEvents(new UntradableListener(this),this);
        getServer().getPluginManager().registerEvents(new DeathListener(this),this);
        getCommand("adminscissors").setExecutor(new CommandHandler());
        getCommand("scissors").setExecutor(new CommandHandler());
        getCommand("makeuntradable").setExecutor(new CommandHandler());
    }
}

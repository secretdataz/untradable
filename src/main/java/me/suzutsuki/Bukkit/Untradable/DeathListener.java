package me.suzutsuki.Bukkit.Untradable;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class DeathListener implements Listener {

    UntradablePlugin pl;
    public DeathListener(UntradablePlugin plugin)
    {
        pl=plugin;
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event)
    {
        //if(event.getEntity().hasPermission("untradable.deathdrop"))return;
        //Bukkit.getConsoleSender().sendMessage("[Untradable]" + event.getEntity().getPlayer().getDisplayName() + " died. Preparing for item restore.");
        Player p = event.getEntity();
        //p.sendMessage("[Untradable] Preparing for item restoration.");
        ItemStack[] content = p.getInventory().getContents();
        ArrayList<ItemStack> stacks = new ArrayList<ItemStack>();
        for(ItemStack itemStack : content)
        {
            if(itemStack != null)
            {
                List<String> lore = itemStack.getItemMeta().getLore();
                if(lore != null)
                {
                    if(lore.contains(UntradablePlugin.UntradableLore))
                    {
                        stacks.add(itemStack);
                        event.getDrops().remove(itemStack);
                    }
                }
            }
        }

        Bukkit.getScheduler().scheduleSyncDelayedTask(pl,new ItemReturnTask(p,stacks.toArray(new ItemStack[stacks.size()])));
    }
}
